#!/usr/bin/python

import sys

pointerString = "->"
nodes = {}
firstLine = True

for line in sys.stdin:
    line = line.rstrip('\n')
    pointerSeen = False
    sourceNodes = []
    destNodes = []

    if firstLine:
        maxNode, maxLines = line.split(" ")
        firstLine = False
       
        maxNode = int(maxNode)
        maxLines = int(maxLines) 
        for i in range(maxNode):
            nodes[i] = "0" * maxNode

        continue

    # process lines into node arrays
    for word in line.split(" "):

        if word == pointerString:
            pointerSeen = True
            continue

        elif pointerSeen == True:
            destNodes.append(int(word))

        else:
            sourceNodes.append(int(word))

    # process line into node{}
    for source in sourceNodes:
        for dest in destNodes:
            new = nodes[source][:dest] + "1" + nodes[source][(dest + 1):]
            nodes[source] = new

# pint them out
for key in sorted(nodes):
    print nodes[key]
